package ru.shokin.tm.service;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.repository.ProjectRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.shokin.tm.constant.ActionConst.*;

public class ProjectService extends AbstractService {

    private final ProjectRepository projectRepository;

    private final UserService userService = UserService.getInstance();

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    private ProjectService() {
        this.projectRepository = ProjectRepository.getInstance();
    }

    private static ProjectService instance = null;

    public static ProjectService getInstance() {
        if (instance == null)
            instance = new ProjectService();
        return instance;
    }

    public Project create(final Long userId, final String name, final String description) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(userId, name, description);
    }

    public Project update(final Long userId, final Long id, final String name, final String description) throws ProjectNotFoundException {
        if (userId == null) return null;
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(userId, id, name, description);
    }

    public void clear() {
        projectRepository.clear();
    }

    public Project removeById(final Long userId, final Long id) throws ProjectNotFoundException {
        if (userId == null) return null;
        if (id == null) return null;
        return projectRepository.removeById(userId, id);
    }

    public Project removeByName(final Long userId, final String name) throws ProjectNotFoundException {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(userId, name);
    }

    public Project findById(final Long userId, final Long id) throws ProjectNotFoundException {
        return projectRepository.findById(userId, id);
    }

    public Project findByName(final Long userId, final String name) throws ProjectNotFoundException {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(userId, name);
    }

    public List<Project> findByNameFromMap(final String name) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) return Collections.emptyList();
        return projectRepository.findByNameFromMap(name);
    }

    public List<Project> findAll(final Long userId) {
        if (userId == null) return Collections.emptyList();
        return projectRepository.findAll(userId);
    }

    public void createProject() {
        System.out.println("Create project");
        System.out.println(ACTION_ENTER_PROJECT_NAME);
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        if (create(userId, name, description) == null) {
            System.out.println(ACTION_FAILED);
            System.out.println("Sorry, we cannot create project with null argument.");
            System.out.println("Please, create project again :)");
            return;
        }
        logger.info("Project created --> name: {}", name);
        System.out.println(ACTION_SUCCESS);
    }

    public void clearProject() {
        System.out.println("Clear project");
        clear();
        System.out.println(ACTION_SUCCESS);
        logger.info("Projects clear");
    }

    public void listProject() {
        System.out.println("Projects list");
        final Long userId = userService.currentUser.getId();
        viewProjects(findAll(userId));
        System.out.println(ACTION_SUCCESS);
    }

    public void viewProjectById() throws ProjectNotFoundException {
        System.out.println(ACTION_ENTER_PROJECT_ID);
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Project project = findById(userId, id);
        viewProject(project);
    }

    public void viewProjectByName() throws ProjectNotFoundException {
        System.out.println(ACTION_ENTER_PROJECT_NAME);
        final String name = scanner.nextLine();
        viewProjectsByName(findByNameFromMap(name));
    }

    public void updateProjectById() throws ProjectNotFoundException {
        System.out.println("Update project by id");
        System.out.println(ACTION_ENTER_PROJECT_ID);
        final Long userId = userService.currentUser.getId();
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = findById(userId, id);
        if (project == null) {
            System.out.println(ACTION_FAILED);
            return;
        }
        System.out.println(ACTION_ENTER_PROJECT_NAME);
        final String name = scanner.nextLine();
        System.out.println("Please, enter project description:");
        final String description = scanner.nextLine();
        update(userId, id, name, description);
        logger.info("Project updated --> id: {} ", id);
        System.out.println(ACTION_SUCCESS);
    }

    public void updateProjectByName() throws ProjectNotFoundException {
        System.out.println("Update project by name");
        System.out.println(ACTION_ENTER_PROJECT_NAME);
        final Long userId = userService.currentUser.getId();
        final String projectName = scanner.nextLine();
        final Project project = findByName(userId, projectName);
        if (project == null) {
            System.out.println(ACTION_FAILED);
            return;
        }
        System.out.println("Please, enter new project name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter new project description:");
        final String description = scanner.nextLine();
        update(userId, project.getId(), name, description);
        logger.info("Project updated --> name: {} ", name);
        System.out.println(ACTION_SUCCESS);
    }

    public void removeProjectById() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("Remove project by id");
        System.out.println(ACTION_ENTER_PROJECT_ID);
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Project project = removeById(userId, id);
        if (project == null) {
            System.out.println(ACTION_FAILED);
        } else {
            projectTaskService.removeProjectWithTasks(userId, id);
            System.out.println(ACTION_SUCCESS);
        }
        logger.info("Project removed --> id: {} ", id);
    }

    public void removeProjectByName() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("Remove project by name");
        System.out.println(ACTION_ENTER_PROJECT_NAME);
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        final Project project = removeByName(userId, name);
        if (project == null) {
            System.out.println(ACTION_FAILED);
        } else {
            final long projectId = project.getId();
            projectTaskService.removeProjectWithTasks(userId, projectId);
            System.out.println(ACTION_SUCCESS);
        }
        logger.info("Project removed --> name: {} ", name);
    }

    private void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("View project");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("***");
        System.out.println("User ID: " + project.getUserId());
        System.out.println(ACTION_SUCCESS);
    }

    public void viewProjects(List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        projects.sort(Comparator.comparing(Project::getName));
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ": " + project.getId() + " " + project.getName());
            index++;
        }
    }

    public void viewProjectsByName(List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        for (final Project project : projects) {
            System.out.println("ID: " + project.getId());
            System.out.println("NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("User ID: " + project.getUserId());
            System.out.println();
        }
    }

}