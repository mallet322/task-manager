package ru.shokin.tm.constant;

public class ActionConst {

    private ActionConst() {
    }

    public static final String ACTION_SUCCESS = "Success";
    public static final String ACTION_FAILED = "Failed";

    public static final String ACTION_ENTER_PROJECT_NAME = "Please, enter project name:";
    public static final String ACTION_ENTER_PROJECT_ID = "Please, enter project id:";
    public static final String ACTION_PROJECT_NOT_FOUND_ID = "Project not found by id: ";
    public static final String ACTION_PROJECT_NOT_FOUND_NAME= "Project not found by name: ";


    public static final String ACTION_ENTER_TASK_NAME = "Please, enter task name:";
    public static final String ACTION_ENTER_TASK_ID = "Please, enter task id:";
    public static final String ACTION_TASK_NOT_FOUND_ID = "Task not found by id: ";
    public static final String ACTION_TASK_NOT_FOUND_NAME = "Task not found by name: ";

    public static final String ACTION_ENTER_USER_NAME = "Please, enter user name:";
    public static final String ACTION_ENTER_USER_LOGIN = "Please, enter user login:";
    public static final String ACTION_ENTER_USER_PASS = "Please, enter user password:";

}