package ru.shokin.tm.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractRepository<E> {

    public final Logger logger = LogManager.getLogger(AbstractRepository.class);

    public final List<E> entities = new ArrayList<>();

    public final Map<String, List<E>> entityMap = new HashMap<>();

    public abstract Long getUserId(E entity);

    public abstract String getName(E entity);

    public void addToMap(E entity) {
        String entityName = getName(entity);
        List<E> entityList = new ArrayList<>();
        if (entityMap.get(entityName) == null) {
            entityList.add(entity);
            entityMap.put(entityName, entityList);
        } else {
            entityList = entityMap.get(entityName);
            entityList.add(entity);
        }
    }

    public List<E> findAll(final Long userId) {
        logger.trace("findAll --> userId: {}", userId);
        final List<E> result = new ArrayList<>();
        for (final E entity : entities) {
            final Long idUser = getUserId(entity);
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(entity);
        }
        return result;
    }

    public void clear() {
        entities.clear();
        entityMap.clear();
    }

}