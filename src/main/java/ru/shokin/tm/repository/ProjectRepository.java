package ru.shokin.tm.repository;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.exception.ProjectNotFoundException;

import java.util.List;

import static ru.shokin.tm.constant.ActionConst.ACTION_PROJECT_NOT_FOUND_ID;
import static ru.shokin.tm.constant.ActionConst.ACTION_PROJECT_NOT_FOUND_NAME;


public class ProjectRepository extends AbstractRepository<Project> {

    private static ProjectRepository instance = null;

    private ProjectRepository() {
    }

    public static ProjectRepository getInstance() {
        if (instance == null) {
            instance = new ProjectRepository();
        }
        return instance;
    }

    public Project create(final Long userId, final String name, final String description) {
        logger.trace("project create --> userId: {}, name: {}, description: {}", userId, name, description);
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        entities.add(project);
        addToMap(project);
        return project;
    }

    public Project update(final Long userId, final Long id, final String name, final String description) throws ProjectNotFoundException {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }


    public Project removeById(final Long userId, final Long id) throws ProjectNotFoundException {
        logger.trace("project removeById --> userId: {}, id: {}", userId, id);
        final Project project = findById(userId, id);
        if (project == null) return null;
        entities.remove(project);
        String name = project.getName();
        entityMap.remove(name);
        return project;
    }

    public Project removeByName(final Long userId, final String name) throws ProjectNotFoundException {
        logger.trace("project removeByName --> userId: {}, name: {}", userId, name);
        final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project);
        entityMap.remove(name);
        return project;
    }

    public Project findById(final Long userId, final Long id) throws ProjectNotFoundException {
        logger.trace("project findById --> userId: {}, id: {}", userId, id);
        for (final Project project : entities) {
            if (project.getUserId().equals(userId) && project.getId().equals(id)) return project;
            else throw new ProjectNotFoundException(ACTION_PROJECT_NOT_FOUND_ID + id);
        }
        return null;
    }

    public Project findByName(final Long userId, final String name) throws ProjectNotFoundException {
        logger.trace("project findByName --> userId: {}, name: {}", userId, name);
        for (final Project project : entities) {
            if (project.getUserId().equals(userId) && project.getName().equals(name)) return project;
            else throw new ProjectNotFoundException(ACTION_PROJECT_NOT_FOUND_NAME + name);
        }
        return null;
    }

    public List<Project> findByNameFromMap(final String name) throws ProjectNotFoundException {
        logger.trace("project findByNameFromMap --> name: {}", name);
        List<Project> projectsList = entityMap.get(name);
        if (projectsList == null) {
            throw new ProjectNotFoundException(ACTION_PROJECT_NOT_FOUND_NAME + name);
        }
        return projectsList;
    }

    public String getName(Project project) {
        if (project == null) return null;
        return project.getName();
    }

    public Long getUserId(Project project) {
        if (project == null) return null;
        return project.getUserId();
    }

}