package ru.shokin.tm.observer.listener;

import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;

public interface Listener {

    void execute(final String command) throws ProjectNotFoundException, TaskNotFoundException;

}