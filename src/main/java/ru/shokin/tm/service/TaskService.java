package ru.shokin.tm.service;

import ru.shokin.tm.entity.Task;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static ru.shokin.tm.constant.ActionConst.*;

public class TaskService extends AbstractService {

    private final TaskRepository taskRepository;

    private final UserService userService = UserService.getInstance();

    public TaskService() {
        this.taskRepository = TaskRepository.getInstance();
    }

    private static TaskService instance = null;

    public static TaskService getInstance() {
        if (instance == null)
            instance = new TaskService();
        return instance;
    }

    public Task create(final Long userId, final String name, final String description) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(userId, name, description);
    }

    public Task update(final Long userId, final Long id, final String name, final String description) throws TaskNotFoundException {
        if (userId == null) return null;
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(userId, id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task removeById(final Long userId, final Long id) throws TaskNotFoundException {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.removeById(userId, id);
    }

    public Task removeByName(final Long userId, final String name) throws TaskNotFoundException {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(userId, name);
    }

    public Task findById(final Long userId, final Long id) throws TaskNotFoundException {
        if (userId == null) return null;
        if (id == null) return null;
        return taskRepository.findById(userId, id);
    }

    public Task findByName(final Long userId, final String name) throws TaskNotFoundException {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(userId, name);
    }

    public List<Task> findByNameFromMap(String name) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) return Collections.emptyList();
        return taskRepository.findByNameFromMap(name);
    }

    public List<Task> findAll(final Long userId) {
        if (userId == null) return Collections.emptyList();
        return taskRepository.findAll(userId);
    }

    public void createTask() {
        System.out.println("Create task");
        System.out.println(ACTION_ENTER_TASK_NAME);
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        System.out.println("Please, enter task description:");
        final String description = scanner.nextLine();
        if (create(userId, name, description) == null) {
            System.out.println(ACTION_FAILED);
            System.out.println("Sorry, we cannot create project with null argument.");
            System.out.println("Please, create project again :)");
            return;
        }
        System.out.println(ACTION_SUCCESS);
    }

    public void clearTask() {
        System.out.println("Clear task");
        clear();
        System.out.println(ACTION_SUCCESS);
    }

    public void listTask() {
        System.out.println("Tasks list");
        final Long userId = userService.currentUser.getId();
        viewTasks(findAll(userId));
        System.out.println("ok");
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("View task");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("***");
        System.out.println("User ID: " + task.getUserId());
        System.out.println(ACTION_SUCCESS);

    }

    public void viewTaskById() throws TaskNotFoundException {
        System.out.println(ACTION_ENTER_TASK_ID);
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Task task = findById(userId, id);
        viewTask(task);
    }

    public void viewTaskByName() throws TaskNotFoundException {
        System.out.println(ACTION_ENTER_TASK_NAME);
        final String name = scanner.nextLine();
        viewTasksByName(findByNameFromMap(name));
    }

    public void updateTaskById() throws TaskNotFoundException {
        System.out.println("Update task by id");
        System.out.println(ACTION_ENTER_TASK_ID);
        final Long userId = userService.currentUser.getId();
        final long id = Long.parseLong(scanner.nextLine());
        final Task task = findById(userId, id);
        if (task == null) {
            System.out.println(ACTION_FAILED);
            return;
        }
        System.out.println(ACTION_ENTER_TASK_NAME);
        final String name = scanner.nextLine();
        System.out.println("Please, enter new task description:");
        final String description = scanner.nextLine();
        update(userId, id, name, description);
        System.out.println(ACTION_SUCCESS);
    }

    public void updateTaskByName() throws TaskNotFoundException {
        System.out.println("Update task by name");
        System.out.println(ACTION_ENTER_TASK_NAME);
        final Long userId = userService.currentUser.getId();
        final String taskName = scanner.nextLine();
        final Task task = findByName(userId, taskName);
        if (task == null) {
            System.out.println(ACTION_FAILED);
            return;
        }
        System.out.println("Please, enter new task name:");
        final String name = scanner.nextLine();
        System.out.println("Please, enter new task description:");
        final String description = scanner.nextLine();
        update(userId, task.getId(), name, description);
        System.out.println(ACTION_SUCCESS);
    }

    public void removeTaskById() throws TaskNotFoundException {
        System.out.println("Remove task by id");
        System.out.println(ACTION_ENTER_TASK_ID);
        final Long userId = userService.currentUser.getId();
        final long id = scanner.nextLong();
        final Task task = removeById(userId, id);
        if (task == null) {
            System.out.println(ACTION_FAILED);
        } else {
            System.out.println(ACTION_SUCCESS);
        }
    }

    public void removeTaskByName() throws TaskNotFoundException {
        System.out.println("Remove task by name");
        System.out.println(ACTION_ENTER_TASK_NAME);
        final Long userId = userService.currentUser.getId();
        final String name = scanner.nextLine();
        final Task task = removeByName(userId, name);
        if (task == null) {
            System.out.println(ACTION_FAILED);
        } else {
            System.out.println(ACTION_SUCCESS);
        }
    }

    public void viewTasks(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        tasks.sort(Comparator.comparing(Task::getName));
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ": " + task.getId() + " " + task.getName());
            index++;
        }
    }

    public void viewTasksByName(List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Sorry, there are no tasks in the program...");
            return;
        }
        for (final Task task : tasks) {
            System.out.println("ID: " + task.getId());
            System.out.println("NAME: " + task.getName());
            System.out.println("DESCRIPTION: " + task.getDescription());
            System.out.println("User ID: " + task.getUserId());
            System.out.println();
        }
    }

}