package ru.shokin.tm.observer.publisher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.observer.listener.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static ru.shokin.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher {

    List<Listener> subscribers = new ArrayList<>();

    private static final Logger logger = LogManager.getLogger(PublisherImpl.class);

    @Override
    public void addListener(Listener listener) {
        subscribers.add(listener);
    }

    @Override
    public void removeListener(Listener listener) {
        subscribers.remove(listener);
    }

    @Override
    public void notifyListener(final String command) throws TaskNotFoundException, ProjectNotFoundException {
        for (Listener listener : subscribers) {
            listener.execute(command);
        }

    }

    @Override
    public void run() {

        final Scanner scanner = new Scanner(System.in);
        String command = "";

        while (!EXIT.equals(command)) {
            command = scanner.nextLine();
            try {
                notifyListener(command);
            } catch (ProjectNotFoundException | TaskNotFoundException exception) {
                logger.error("Error --> msg: {}", exception.getMessage());
            }
            System.out.println();
        }

    }

}