package ru.shokin.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public abstract class AbstractService {

    protected final Scanner scanner = new Scanner(System.in);

    public static final Logger logger = LogManager.getLogger(AbstractService.class);

}
