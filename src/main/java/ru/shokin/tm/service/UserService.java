package ru.shokin.tm.service;

import ru.shokin.tm.entity.User;
import ru.shokin.tm.enumerated.Role;
import ru.shokin.tm.repository.UserRepository;
import ru.shokin.tm.util.HashUtil;

import java.util.List;

import static ru.shokin.tm.constant.ActionConst.*;

public class UserService extends AbstractService {

    public User currentUser;

    private final UserRepository userRepository;

    public UserService() {
        this.userRepository = UserRepository.getInstance();
    }

    private static UserService instance = null;

    public static UserService getInstance(){
        if (instance == null) instance = new UserService();
        return instance;
    }

    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (role == null) return null;
        return userRepository.create(login, password, role);
    }

    public User update(final String login, final Role role, final String firstName, final String lastName) {
        if (login == null || login.isEmpty()) return null;
        if (role == null) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        return userRepository.update(login, role, firstName, lastName);
    }

    public User update(final String login, final String password, final Role role,
                       final String firstName, final String lastName
    ) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (role == null) return null;
        if (firstName == null || firstName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        return userRepository.update(login, password, role, firstName, lastName);
    }

    public void addUserInformation(final String login, final String firstName, final String lastName) {
        if (login == null || login.isEmpty()) return;
        if (firstName == null || firstName.isEmpty()) return;
        if (lastName == null || lastName.isEmpty()) return;
        userRepository.addUserInformation(login, firstName, lastName);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public void clear() {
        userRepository.clear();
    }

    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }

    public void changePassword(final String login, final String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        userRepository.changePassword(login, password);
    }

    public void createUser() {
        System.out.println("Create user");
        System.out.println(ACTION_ENTER_USER_NAME);
        final String login = scanner.nextLine();
        System.out.println(ACTION_ENTER_USER_PASS);
        final String password = scanner.nextLine();
        final Role role = grantRoles();
        final User findUser = findByLogin(login);
        if (findUser != null) {
            System.out.println("Sorry, this login is already busy.");
            System.out.println("Come back with a new username :)");
            return;
        }
        final User user = create(login, password, role);
        if (user == null) {
            System.out.println(ACTION_FAILED);
            System.out.println("Sorry, we cannot create user with null argument.");
            System.out.println("Please, create user again :)");
            return;
        }
        System.out.println(ACTION_SUCCESS);
    }

    public void clearUser() {
        System.out.println("Remove all users");
        clear();
        System.out.println(ACTION_SUCCESS);
    }

    public void listUser() {
        System.out.println("Users list");
        int index = 1;
        for (final User user : findAll()) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin() + ", " + user.getRole());
            index++;
        }
        System.out.println(ACTION_SUCCESS);
    }

    private void viewUser(final User user) {
        if (user == null) return;
        System.out.println("View user");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("ROLE: " + user.getRole());
        System.out.println();
        System.out.println("FIRST_NAME: " + user.getFirstName());
        System.out.println("LAST_NAME: " + user.getLastName());
    }

    public void viewUserByLogin() {
        System.out.println(ACTION_ENTER_USER_LOGIN);
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        if (user == null) {
            System.out.println(ACTION_FAILED);
            System.out.println("Sorry, we cannot display user with this login or null argument.");
            return;
        }
        viewUser(user);
    }

    public void updateUserByLogin() {
        System.out.println("Update user by login");
        System.out.println(ACTION_ENTER_USER_LOGIN);
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        if (user == null) {
            System.out.println(ACTION_FAILED);
            System.out.println("Sorry, we cannot updater user with this login or null argument.");
            return;
        }
        System.out.println("Please, enter new password:");
        final String password = scanner.nextLine();
        System.out.println("Please, enter new first name:");
        final String firstName = scanner.nextLine();
        System.out.println("Please, enter new last name:");
        final String lastName = scanner.nextLine();
        final Role role = grantRoles();
        update(login, password, role, firstName, lastName);
        System.out.println(ACTION_SUCCESS);
    }

    public void removeUserByLogin() {
        System.out.println("Remove user");
        System.out.println(ACTION_ENTER_USER_LOGIN);
        final String login = scanner.nextLine();
        final User user = removeByLogin(login);
        if (user == null) {
            System.out.println(ACTION_FAILED);
        } else {
            System.out.println(ACTION_SUCCESS);
        }
    }

    public void addUserInformation() {
        System.out.println("Add user information");
        System.out.println(ACTION_ENTER_USER_LOGIN);
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        if (user == null) {
            System.out.println(ACTION_FAILED);
            System.out.println("Sorry, we cannot add user info with this login or null argument.");
            return;
        }
        System.out.println("Please, enter your first name:");
        final String firstName = scanner.nextLine();
        System.out.println("Please, enter your last name:");
        final String lastName = scanner.nextLine();
        addUserInformation(login, firstName, lastName);
        System.out.println(ACTION_SUCCESS);
    }

    private Role grantRoles() {
        System.out.println("*********");
        System.out.println("If you want to install the admin role, enter - ADMIN");
        System.out.println("All new users are assigned the USER role by default");
        final String grant = scanner.nextLine();

        try {
            return Role.valueOf(grant);
        } catch (IllegalArgumentException | NullPointerException exception) {
            System.out.println("Role does not exist.");
            System.out.println("USER role set by default");
            return Role.USER;
        }
    }

    public int authenticationUser() {
        System.out.println("User authentication");
        System.out.println(ACTION_ENTER_USER_LOGIN);
        final String login = scanner.nextLine();
        final User user = findByLogin(login);
        if (user == null) {
            System.out.println("Login is not found");
            return -1;
        }
        System.out.println(ACTION_ENTER_USER_PASS);
        final String password = HashUtil.encryptMD5(scanner.nextLine());
        if (user.getPassword().equals(password)) {
            System.out.println("Welcome back, " + user.getLogin() + "!");
            System.out.println("Get start working with the program :)");
            currentUser = user;
        } else {
            System.out.println(ACTION_FAILED);
            System.out.println("Invalid password.");
            return -1;
        }
        return 0;
    }

    public void endingSession() {
        currentUser = null;
        System.out.println("Ending current session...");
        System.out.println("Please, come back again :)");
        System.out.println();
        System.out.println("******");
        System.out.println("Please log in!");
    }

    public void viewCurrentUser() {
        viewUser(currentUser);
        System.out.println(ACTION_SUCCESS);
    }

    public void updateCurrentUser() {
        System.out.println("Update your information");
        final String login;
        login = currentUser.getLogin();
        System.out.println("Please, enter new first name:");
        final String firstName = scanner.nextLine();
        System.out.println("Please, enter new last name:");
        final String lastName = scanner.nextLine();
        final Role role = grantRoles();
        update(login, role, firstName, lastName);
        System.out.println(ACTION_SUCCESS);
    }

    public void changePassword() {
        System.out.println("Change password");
        final String login = currentUser.getLogin();
        System.out.println("Please, enter your password:");
        final String oldPassword = HashUtil.encryptMD5(scanner.nextLine());
        if (oldPassword.equals(currentUser.getPassword())) {
            System.out.println(ACTION_SUCCESS);
        } else {
            System.out.println(ACTION_FAILED);
            System.out.println("Invalid password");
        }
        System.out.println("Please, enter new password:");
        final String newPassword = scanner.nextLine();
        changePassword(login, newPassword);
        System.out.println(ACTION_SUCCESS);
    }

}