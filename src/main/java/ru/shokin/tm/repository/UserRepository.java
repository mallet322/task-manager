package ru.shokin.tm.repository;

import ru.shokin.tm.enumerated.Role;
import ru.shokin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance = null;

    private UserRepository() {
    }

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    private final List<User> users = new ArrayList<>();

    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        users.add(user);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        final User user = new User(login, password, role);
        users.add(user);
        return user;
    }

    public User update(final String login, Role role,
                       final String firstName, final String lastName)
    {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setRole(role);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public User update(final String login, final String password, Role role,
                       final String firstName, final String lastName
    ) {
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setPassword(password);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return user;
    }

    public void addUserInformation(final String login, final String firstName, final String lastName) {
        final User user = findByLogin(login);
        if (user == null) return;
        user.setFirstName(firstName);
        user.setLastName(lastName);
    }

    public List<User> findAll() {
        return users;
    }

    public User findByLogin(final String login) {
        for (final User user : users) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public User findById(final Long id) {
        for (final User user : users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    public void clear() {
        users.clear();
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;
    }

    public void changePassword(final String login, final String password) {
        final User user = findByLogin(login);
        if (user == null) return;
        user.setPassword(password);
    }

}