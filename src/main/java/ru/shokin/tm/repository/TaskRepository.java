package ru.shokin.tm.repository;

import ru.shokin.tm.entity.Task;
import ru.shokin.tm.exception.TaskNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static ru.shokin.tm.constant.ActionConst.*;


public class TaskRepository extends AbstractRepository<Task> {

    private static TaskRepository instance = null;

    private TaskRepository() {
    }

    public static TaskRepository getInstance() {
        if (instance == null) {
            instance = new TaskRepository();
        }
        return instance;
    }

    public Task create(final Long userId, final String name, final String description) {
        logger.trace("task create --> userId: {}, name: {}, description: {}", userId, name, description);
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        entities.add(task);
        addToMap(task);
        return task;
    }

    public Task update(final Long userId, final Long id, String name, final String description) throws TaskNotFoundException {
        logger.trace("task update --> userId: {}, id: {}, name: {}, description: {}", userId, id, name, description);
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public Task removeById(final Long userId, final Long id) throws TaskNotFoundException {
        logger.trace("task removeById --> userId: {}, id: {}", userId, id);
        final Task task = findById(userId, id);
        entities.remove(task);
        String name = task.getName();
        entityMap.remove(name);
        return task;
    }

    public Task removeByName(final Long userId, final String name) throws TaskNotFoundException {
        logger.trace("task removeByName --> userId: {}, name: {}", userId, name);
        final Task task = findByName(userId, name);
        entities.remove(task);
        entityMap.remove(name);
        return task;
    }

    public Task findById(final Long userId, final Long id) throws TaskNotFoundException {
        logger.trace("task findById --> userId: {}, id: {}", userId, id);
        for (final Task task : entities) {
            if (task.getUserId().equals(userId) && task.getId().equals(id)) return task;
            else throw new TaskNotFoundException(ACTION_TASK_NOT_FOUND_ID + id);
        }
        return null;
    }

    public Task findByName(final Long userId, final String name) throws TaskNotFoundException {
        logger.trace("task findByName --> userId: {}, name: {}", userId, name);
        for (final Task task : entities) {
            if (task.getUserId().equals(userId) && task.getName().equals(name)) return task;
            else throw new TaskNotFoundException(ACTION_TASK_NOT_FOUND_NAME + name);
        }
        return null;
    }

    public List<Task> findByNameFromMap(final String name) throws TaskNotFoundException {
        logger.trace("task findByNameFromMap --> name: {}", name);
        List<Task> tasksList = entityMap.get(name);
        if (tasksList == null) {
            throw new TaskNotFoundException(ACTION_TASK_NOT_FOUND_NAME + name);
        }
        return tasksList;
    }


    public Task findByProjectIdAndId(final Long projectId, final Long id) throws TaskNotFoundException {
        logger.trace("task findByProjectIdAndId --> projectId: {}, id: {}", projectId, id);
        for (final Task task : entities) {
            final Long idProject = task.getProjectId();
            if (idProject == null || !idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
            else throw new TaskNotFoundException(ACTION_FAILED + ACTION_TASK_NOT_FOUND_ID + id);
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        logger.trace("task findByProjectIdAndId --> projectId: {}", projectId);
        final List<Task> result = new ArrayList<>();
        for (final Task task : entities) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Long getUserId(Task task) {
        if (task == null) return null;
        return task.getUserId();
    }

    public String getName(Task task) {
        if (task == null) return null;
        return task.getName();
    }

}