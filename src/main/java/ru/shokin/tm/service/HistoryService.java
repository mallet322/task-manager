package ru.shokin.tm.service;

import ru.shokin.tm.repository.HistoryRepository;

public class HistoryService {

    private final HistoryRepository historyRepository;

    public HistoryService() {
        this.historyRepository = HistoryRepository.getInstance();
    }

    private static HistoryService instance = null;

    public static HistoryService getInstance() {
        if (instance == null)
            instance = new HistoryService();
        return instance;
    }

    public void create(final String command) {
        if (command == null || command.isEmpty()) return;
        historyRepository.create(command);
    }

    public void clear() {
        historyRepository.clear();
    }

    public void viewHistory() {
        historyRepository.viewHistory();
    }

    public void addCommand(final String command) {
        create(command);
    }

    public void historyList() {
        System.out.println("Last entered 10 commands");
        viewHistory();
        System.out.println("ok");
    }

}