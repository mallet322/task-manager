package ru.shokin.tm.service;

import ru.shokin.tm.entity.Project;
import ru.shokin.tm.entity.Task;
import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.repository.ProjectRepository;
import ru.shokin.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

import static ru.shokin.tm.constant.ActionConst.*;

public class ProjectTaskService extends AbstractService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    private final TaskService taskService = TaskService.getInstance();

    public ProjectTaskService() {
        this.projectRepository = ProjectRepository.getInstance();
        this.taskRepository = TaskRepository.getInstance();
    }

    private static ProjectTaskService instance = null;

    public static ProjectTaskService getInstance() {
        if (instance == null) instance = new ProjectTaskService();
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public void addTaskToProject(final Long userId, final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project = projectRepository.findById(userId, projectId);
        if (project == null) return;
        final Task task = taskRepository.findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    public void removeTaskFromProject(final Long projectId, final Long taskId) throws TaskNotFoundException {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

    public void removeProjectWithTasks(final Long userId, final Long projectId) throws TaskNotFoundException {
        final List<Task> tasks = findAllByProjectId(projectId);
        if (tasks == null) return;
        for (final Task task : tasks) {
            final long taskId = task.getId();
            taskRepository.removeById(userId, taskId);
        }
    }

    public void addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("Add task to project by ids");
        final Long userId = UserService.getInstance().currentUser.getId();
        System.out.println("Please, enter project id:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("Please, enter task id:");
        final long taskId = Long.parseLong(scanner.nextLine());
        addTaskToProject(userId, projectId, taskId);
        System.out.println(ACTION_SUCCESS);
        logger.info("Task added to project --> projectId: {}, taskId: {} ", projectId, taskId);
    }

    public void removeTaskFromProjectByIds() throws TaskNotFoundException {
        System.out.println("Remove task from project by ids");
        System.out.println(ACTION_ENTER_PROJECT_ID);
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println(ACTION_ENTER_TASK_ID);
        final long taskId = Long.parseLong(scanner.nextLine());
        removeTaskFromProject(projectId, taskId);
        System.out.println(ACTION_SUCCESS);
        logger.info("Task removed from project --> projectId: {}, taskId: {} ", projectId, taskId);
    }

    public void listTaskByProjectId() {
        System.out.println("List task by project");
        System.out.println(ACTION_ENTER_PROJECT_NAME);
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = findAllByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("Sorry, but this project has no tasks...");
        } else {
            taskService.viewTasks(tasks);
            System.out.println(ACTION_SUCCESS);
        }
    }

}