package ru.shokin.tm.observer.listener;

import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.service.HistoryService;
import ru.shokin.tm.service.ProjectService;

import static ru.shokin.tm.constant.TerminalConst.*;

public class ProjectListener implements Listener {

    private final ProjectService projectService = ProjectService.getInstance();

    private final HistoryService historyService = HistoryService.getInstance();

    @Override
    public void execute(String command) throws ProjectNotFoundException, TaskNotFoundException {
        historyService.addCommand(command);

        switch (command) {
            case PROJECT_CREATE:
                projectService.createProject();
                return;
            case PROJECT_CLEAR:
                projectService.clearProject();
                return;
            case PROJECT_LIST:
                projectService.listProject();
                return;
            case PROJECT_VIEW_BY_ID:
                projectService.viewProjectById();
                return;
            case PROJECT_VIEW_BY_NAME:
                projectService.viewProjectByName();
                return;
            case PROJECT_REMOVE_BY_NAME:
                projectService.removeProjectByName();
                return;
            case PROJECT_REMOVE_BY_ID:
                projectService.removeProjectById();
                return;
            case PROJECT_UPDATE_BY_ID:
                projectService.updateProjectById();
                return;
            case PROJECT_UPDATE_BY_NAME:
                projectService.updateProjectByName();
                return;
            default:
                break;

        }
    }
}