package ru.shokin.tm.observer.listener;

import ru.shokin.tm.service.HistoryService;
import ru.shokin.tm.service.UserService;

import static ru.shokin.tm.constant.TerminalConst.*;

public class UserListener implements Listener {

    private final UserService userService = UserService.getInstance();

    private final HistoryService historyService = HistoryService.getInstance();

    @Override
    public void execute(String command) {
        historyService.addCommand(command);

        switch (command) {

            case LOGIN:
                userService.authenticationUser();
                return;
            case LOGOFF:
                userService.endingSession();
                return;
            case USER_CREATE:
                userService.createUser();
                return;
            case USER_CLEAR:
                userService.clearUser();
                return;
            case USER_LIST:
                userService.listUser();
                return;
            case USER_ADD_INFORMATION:
                userService.addUserInformation();
                return;
            case USER_VIEW_INFORMATION:
                userService.viewCurrentUser();
                return;
            case USER_UPDATE_INFORMATION:
                userService.updateCurrentUser();
                return;
            case USER_CHANGE_PASSWORD:
                userService.changePassword();
                return;
            case USER_VIEW_BY_LOGIN:
                userService.viewUserByLogin();
                return;
            case USER_REMOVE_BY_LOGIN:
                userService.removeUserByLogin();
                return;
            case USER_UPDATE_BY_LOGIN:
                userService.updateUserByLogin();
                return;

            default:
                break;

        }
    }

}