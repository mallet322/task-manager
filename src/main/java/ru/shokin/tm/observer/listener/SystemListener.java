package ru.shokin.tm.observer.listener;

import ru.shokin.tm.service.HistoryService;
import ru.shokin.tm.service.SystemService;

import static ru.shokin.tm.constant.TerminalConst.*;

public class SystemListener implements Listener {

    private final SystemService systemService = SystemService.getInstance();

    private final HistoryService historyService = HistoryService.getInstance();


    @Override
    public void execute(String command) {
        historyService.addCommand(command);

        switch (command) {

            case VERSION:
                systemService.displayVersion();
                return;
            case ABOUT:
                systemService.displayAbout();
                return;
            case HELP:
                systemService.displayHelp();
                return;
            case HISTORY:
                historyService.historyList();
                return;
            case EXIT:
                systemService.displayExit();
                return;
            default:
                break;

        }
    }

}