package ru.shokin.tm.observer.listener;

import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.service.HistoryService;
import ru.shokin.tm.service.TaskService;

import static ru.shokin.tm.constant.TerminalConst.*;

public class TaskListener implements Listener {

    private final TaskService taskService = TaskService.getInstance();

    private final HistoryService historyService = HistoryService.getInstance();

    @Override
    public void execute(String command) throws TaskNotFoundException {
        historyService.addCommand(command);

        switch (command) {
            case TASK_CREATE:
                taskService.createTask();
                return;
            case TASK_CLEAR:
                taskService.clearTask();
                return;
            case TASK_LIST:
                taskService.listTask();
                return;
            case TASK_VIEW_BY_ID:
                taskService.viewTaskById();
                return;
            case TASK_VIEW_BY_NAME:
                taskService.viewTaskByName();
                return;
            case TASK_REMOVE_BY_NAME:
                taskService.removeTaskByName();
                return;
            case TASK_REMOVE_BY_ID:
                taskService.removeTaskById();
                return;
            case TASK_UPDATE_BY_ID:
                taskService.updateTaskById();
                return;
            case TASK_UPDATE_BY_NAME:
                taskService.updateTaskByName();
                return;

            default:
                break;
        }
    }

}