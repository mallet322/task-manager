package ru.shokin.tm;

import ru.shokin.tm.entity.User;
import ru.shokin.tm.enumerated.Role;
import ru.shokin.tm.observer.listener.*;
import ru.shokin.tm.observer.publisher.Publisher;
import ru.shokin.tm.observer.publisher.PublisherImpl;
import ru.shokin.tm.service.ProjectService;
import ru.shokin.tm.service.SystemService;
import ru.shokin.tm.service.TaskService;
import ru.shokin.tm.service.UserService;


public class Application {

    private final SystemService systemService = SystemService.getInstance();

    private final UserService userService = UserService.getInstance();


    static {
        final User me = UserService.getInstance().create("mallet", "qwerty123", Role.ADMIN);
        final User admin = UserService.getInstance().create("ADMIN", "ADMIN", Role.ADMIN);
        final User user = UserService.getInstance().create("TEST", "TEST", Role.USER);
        ProjectService.getInstance().create(me.getId(), "MY PROJECT", "PROJECT DESC");
        ProjectService.getInstance().create(admin.getId(), "DEMO PROJECT 1", "PROJECT DESC 1");
        ProjectService.getInstance().create(user.getId(), "DEMO PROJECT 2", "PROJECT DESC 2");
        TaskService.getInstance().create(me.getId(), "MY TASK", "TASK DESC");
        TaskService.getInstance().create(admin.getId(), "DEMO TASK 1", "TASK DESC 1");
        TaskService.getInstance().create(user.getId(), "DEMO PROJECT 2", "TASK DESC 2");
    }

    public static void main(final String[] args) {
        final Application app = new Application();
        app.systemService.displayWelcome();
        app.authentication();
        app.process();
    }

    private void authentication() {
        final int login = userService.authenticationUser();
        if (login == -1) {
            System.exit(-1);
        }
    }

    private void process() {
        Publisher subscribers = new PublisherImpl();
        subscribers.addListener(new SystemListener());
        subscribers.addListener(new UserListener());
        subscribers.addListener(new ProjectListener());
        subscribers.addListener(new TaskListener());
        subscribers.addListener(new ProjectTaskListener());
        subscribers.run();
    }

}