package ru.shokin.tm.observer.publisher;

import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.observer.listener.Listener;

public interface Publisher {

    void addListener(Listener listener);

    void removeListener(Listener listener);

    void notifyListener(final String command) throws TaskNotFoundException, ProjectNotFoundException;

    void run();

}