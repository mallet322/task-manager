package ru.shokin.tm.observer.listener;

import ru.shokin.tm.exception.ProjectNotFoundException;
import ru.shokin.tm.exception.TaskNotFoundException;
import ru.shokin.tm.service.HistoryService;
import ru.shokin.tm.service.ProjectTaskService;

import static ru.shokin.tm.constant.TerminalConst.*;

public class ProjectTaskListener implements Listener {

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    private final HistoryService historyService = HistoryService.getInstance();

    @Override
    public void execute(String command) throws TaskNotFoundException, ProjectNotFoundException {
        historyService.addCommand(command);

        switch (command) {
            case TASK_ADD_TO_PROJECT_BY_IDS:
                projectTaskService.addTaskToProjectByIds();
                return;
            case TASK_REMOVE_FROM_PROJECT_BY_IDS:
                projectTaskService.removeTaskFromProjectByIds();
                return;
            case TASK_LIST_BY_PROJECT_ID:
                projectTaskService.listTaskByProjectId();
                return;

            default:
                break;

        }
    }

}