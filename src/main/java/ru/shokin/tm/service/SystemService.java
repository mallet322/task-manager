package ru.shokin.tm.service;

public class SystemService extends AbstractService {

    private static SystemService instance = null;

    public static SystemService getInstance() {
        if (instance == null)
            instance = new SystemService();
        return instance;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public void displayHelp() {
        System.out.println("*** INFO PANEL ***");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.out.println("history - Display list of last entered commands.");
        System.out.println("exit - Closing the application.");
        System.out.println();
        System.out.println("*** PROJECTS ***");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-list  - Display list of projects.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-view-by-name - Display project by name.");
        System.out.println("project-update-by-id - Update project by id");
        System.out.println("project-update-by-id - Update project by name");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-name - Remove project by name");
        System.out.println();
        System.out.println("*** TASKS ***");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-list  - Display list of tasks.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-view-by-name - Display task by name.");
        System.out.println("task-update-by-id - Update task by id");
        System.out.println("task-update-by-id - Update task by name");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-list-by-project-id - Display task list by project id");
        System.out.println("task-add-to-project-by-ids - Add task to project by ids");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids");
        System.out.println();
        System.out.println("*** USERS ***");
        System.out.println("user-create - Create new user by name.");
        System.out.println("user-list  - Display list of users.");
        System.out.println("user-clear - Remove all users.");
        System.out.println("user-add-information - Add user information.");
        System.out.println("user-view-information - Display user information.");
        System.out.println("user-update-information - Update user information");
        System.out.println("user-change-password - Change user password.");
        System.out.println("user-view-by-login - Display user by login.");
        System.out.println("user-remove-by-login - Remove user by login.");
        System.out.println("user-update-by-login - Update user by login.");
    }

    public void displayVersion() {
        System.out.println("Current version: 1.1.5");
    }

    public void displayAbout() {
        System.out.println("Developer: Elias Shokin");
        System.out.println("   Mailto: shokin_is@nlmk.com");
    }

    public void displayError() {
        System.out.println("Error!!! Unknown program arguments...");
    }

    public void displayExit() {
        System.out.println("See you later! :)");
        System.exit(0);
    }

}